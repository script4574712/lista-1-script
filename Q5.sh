#!/bin/bash

data=$(date "+%Y-%m-%d-%H")
dir="/tmp/$data"
mkdir -p "$dir"

cp -r ./* "$dir"

tar -czf "$data.tar.gz" "$dir" && rm -rf "$dir" && mv "$data.tar.gz" ~/
