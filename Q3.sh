#!/bin/bash

# verifica se foram fornecidos os 3 nomes de diretórios como parâmetros

if [ $# -lt 3 ]; then
  echo "Por favor, forneça o nome de 3 diretórios como parâmetros."
  exit 1
fi

dir1=$1
dir2=$2
dir3=$3

echo "Arquivos do diretório $dir1:"
ls -l $dir1

read -p "Pressione Enter para continuar..."

echo "Arquivos do diretório $dir2:"
ls -l $dir2

read -p "Pressione Enter para continuar..."

echo "Arquivos do diretório $dir3:"
ls -l $dir3

read -p "Pressione Enter para sair..."

# Para visualizar: ./meu_script.sh dir1 dir2 dir3
