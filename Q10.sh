#!/bin/bash

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <filename1> <filename2>"
  exit 1
fi

linhas1=$(wc -l < "$1")
linhas2=$(wc -l < "$2")

soma=$((linhas1 + linhas2))

echo "$soma"
