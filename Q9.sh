#!/bin/bash

if [ "$#" -ne 2 ]; then
  echo "uso: $0 <a> <b>"
  exit 1
fi

a=$(($1 + 1))
b=$(($2 + 1))
resultado=$(($a * $b))

echo "$resultado"
