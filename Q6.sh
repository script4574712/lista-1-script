#!/bin/bash

# variavel de substituicao
nome="Joao"
echo "olá, ${nome}!"

# Shell substituicao
echo "Hoje é $(date +%A)"

# substituicao aritmetica
x=5
y=3
echo "$x + $y = $((x + y))"
